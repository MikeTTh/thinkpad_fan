package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

type config struct {
	Wait int
	Pad int
	Levels map[string]int
}

func main() {
	cpath := flag.String("conf", "fan.conf", "Location of config file")
	flag.Parse()

	conf := &config{}

	confF, er := os.Open(*cpath)
	check(er)
	defer func() {
		_ = confF.Close()
	}()
	confstr, er := ioutil.ReadAll(confF)
	check(er)
	confstr1 := strings.Split(string(confstr), "\n")
	conf.Levels = map[string]int{}
	for _, line := range confstr1 {
		words := strings.Split(line, " ")
		if temp, err := strconv.Atoi(words[0]); err == nil {
			conf.Levels[words[1]] = temp
		} else {
			switch words[0] {
			case "wait":
				t, _ := strconv.Atoi(words[1])
				conf.Wait = t
				break
			case "pad":
				t, _ := strconv.Atoi(words[1])
				conf.Pad = t
				break
			}

		}
	}

	var sensors = make([]*os.File, 0)

	fan, e := os.OpenFile("/proc/acpi/ibm/fan", os.O_CREATE|os.O_RDWR, 0644)
	// fan, e := os.OpenFile("./fanspeed.txt", os.O_CREATE|os.O_RDWR, 0644)
	check(e)

	defer func() {
		_, _ = fan.Seek(0, 0)
		_, _ = fan.WriteString("level auto")
		_ = fan.Close()

		for _, temp := range sensors {
			_ = temp.Close()
		}
	}()

	tempIn := regexp.MustCompile("(temp)([0-9])(_input)")
	hwmon := regexp.MustCompile("(hwmon)([0-9])")


	err := filepath.Walk("/sys/class/hwmon", func(path string, info os.FileInfo, er error) error {
		if hwmon.MatchString(path) {
			link, e := filepath.EvalSymlinks(path)
			check(e)
			err := filepath.Walk(link, func(path1 string, info1 os.FileInfo, er1 error) error {
				if info1 != nil && tempIn.MatchString(info1.Name()) {
					f, e := os.Open(path1)
					check(e)
					check(er1)
					sensors = append(sensors, f)
					return nil
				}
				return nil
			})
			check(err)
		}
		return nil
	})
	check(err)

	prevLev := 0
	level := ""
	target := 0

	for {
		temps := make([]int, 0)
		for _, sensor := range sensors {
			_, _ = sensor.Seek(0, 0)
			out, err := ioutil.ReadAll(sensor)
			check(err)
			num, e := strconv.Atoi(strings.ReplaceAll(string(out), "\n", ""))
			check(e)
			temps = append(temps, num)
		}

		max := 0
		for _, t := range temps {
			if max < t {
				max = t
			}
		}
		c := max/1000

		_, _ = fan.Seek(0, 0)

		if c>prevLev || c <= target {
			p := 0
			for lev, temp := range conf.Levels {
				if c >= temp && temp >= p {
					level = lev
					target = temp - conf.Pad
					prevLev = temp
					p = temp
				}
			}
			_, err := fan.WriteString(fmt.Sprintf("level %s\n", level))
			check(err)
		}

		// fmt.Printf("\rlevel %s (%d °C, target: %d °C, prevlev: %d °C)          ", level, c, target, prevLev)

		time.Sleep(5*time.Second)
	}
}
