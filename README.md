# Thinkpad fan control

_**I am not responsible for any damages resulting from using my program, but under normal circumstances everything sould be fine.**_

## Setup
Put this into `/etc/modprobe.d/thinkpad_acpi.conf`:
```
options thinkpad_acpi fan_control=1
```
**Reboot!**  


Build the program:
```
go build fan.go
```
If you want the program to run in the background and/or start on boot, you should link the systemd unit file:
```
sudo ln -s $PWD /opt/
sudo ln -s /opt/thinkpad_fan/thinkpadfan.service /lib/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable thinkpadfan
sudo systemctl start thinkpadfan
```